package com.davud.tutorials.dagger.modules

import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

@Module
object LocalNavigationModule {
    private val cicerone: Cicerone<Router> = Cicerone.create()
    @Provides
    @JvmStatic
    fun getRouter(): Router {
        return cicerone.router
    }

    @Provides
    @JvmStatic
    fun getNavigatorHolder(): NavigatorHolder {
        return cicerone.navigatorHolder
    }
}