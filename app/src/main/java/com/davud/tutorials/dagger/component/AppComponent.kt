package com.davud.tutorials.dagger.component

import android.app.Application
import com.davud.tutorials.MyApplication
import com.davud.tutorials.dagger.modules.ActivityBuildersModule
import com.davud.tutorials.dagger.modules.AppModule
import com.davud.tutorials.dagger.modules.LocalNavigationModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBuildersModule::class,
        AppModule::class,
        LocalNavigationModule::class
    ]   
)
interface AppComponent : AndroidInjector<MyApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}