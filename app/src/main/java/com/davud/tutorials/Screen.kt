package com.davud.tutorials

import androidx.fragment.app.Fragment
import com.davud.tutorials.ui.auth.Example1
import com.davud.tutorials.ui.auth.Example2
import com.davud.tutorials.ui.auth.Example3
import com.davud.tutorials.ui.auth.Example4
import ru.terrakok.cicerone.android.support.SupportAppScreen

class Screen {
    class Ex1(private var str: String) : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return Example1.getInstance(str)
        }
    }

    object Ex2 : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return Example2()
        }
    }

    object Ex3 : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return Example3()
        }
    }

    object Ex4 : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return Example4()
        }
    }
//
//    fun setFragment(fragment: Fragment): SupportAppScreen {
//        fragmentClass = fragment
//        return this
//    }
//    fun setFragment(fragment: Fragment): SupportAppScreen {
//        fragmentClass = fragment
//        return this
//    }
//    fun setFragment(fragment: Fragment): SupportAppScreen {
//        fragmentClass = fragment
//        return this
//    }
//    fun setFragment(fragment: Fragment): SupportAppScreen {
//        fragmentClass = fragment
//        return this
//    }
//    fun setFragment(fragment: Fragment): SupportAppScreen {
//        fragmentClass = fragment
//        return this
//    }
//
//    override fun getFragment(): Fragment? {
//        return fragmentClass
//    }
}
