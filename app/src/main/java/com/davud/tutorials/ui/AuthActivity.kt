package com.davud.tutorials.ui

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.davud.tutorials.R
import com.davud.tutorials.Screen
import com.davud.tutorials.mvp.auth.AuthPresenter
import com.davud.tutorials.mvp.auth.AuthView
import com.davud.tutorials.ui.auth.Example1
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_auth.*
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Replace
import javax.inject.Inject

class AuthActivity : MvpAppCompatActivity(), AuthView,
    BottomNavigationView.OnNavigationItemSelectedListener {
    @InjectPresenter
    lateinit var presenter: AuthPresenter

    @ProvidePresenter
    fun providePresenter(): AuthPresenter { 
        return AuthPresenter(router)
    }

    @Inject
    lateinit var router: Router
    @Inject
    lateinit var navigatorHolder: NavigatorHolder
//    private lateinit var bindingUtil: ActivityAuthBinding
    private val navigator: Navigator = object : SupportAppNavigator(
        this,
        R.id.container
    ) {
        override fun applyCommands(commands: Array<out Command>?) {
            super.applyCommands(commands)
            supportFragmentManager.beginTransaction()
        }

        override fun createFragment(screen: SupportAppScreen?): Fragment {
            if (screen?.fragment is Example1) {
                bottom_navigation.menu.getItem(0).isChecked = true
            }
            return super.createFragment(screen)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
//        bindingUtil = DataBindingUtil.setContentView(this, R.layout.activity_auth)
        navigator.applyCommands(arrayOf(Replace(Screen.Ex1("Something"))))
//        bindingUtil.bottomNavigation.setOnNavigationItemSelectedListener(this)
          bottom_navigation.setOnNavigationItemSelectedListener(this)
    }

    fun onClick(v:View) {
        router.replaceScreen(Screen.Ex1("Something"))
    }

    override fun onResumeFragments() {
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

    override fun show(msg: String?) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.bottom1 -> router.replaceScreen(Screen.Ex1("Something"))
            R.id.bottom2 -> router.replaceScreen(Screen.Ex2)
            R.id.bottom3 -> router.replaceScreen(Screen.Ex3)
            R.id.bottom4 -> router.replaceScreen(Screen.Ex4)
        }
        return true
    }
}
