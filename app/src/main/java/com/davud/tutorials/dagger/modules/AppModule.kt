package com.davud.tutorials.dagger.modules

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides

@Module
object AppModule {

    @Provides
    @JvmStatic
    fun getPreferences(application: Application): SharedPreferences {
        return application.getSharedPreferences("preferences", Context.MODE_PRIVATE)
    }

}