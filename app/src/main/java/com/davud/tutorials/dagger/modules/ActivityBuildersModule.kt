package com.davud.tutorials.dagger.modules

import com.davud.tutorials.ui.AuthActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {
    @ContributesAndroidInjector
    abstract fun authActivity(): AuthActivity
}