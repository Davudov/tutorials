package com.davud.tutorials.mvp.auth

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class AuthPresenter(private val router: Router) : MvpPresenter<AuthView>() {
    override fun onFirstViewAttach() {
        viewState.show("Presenter")
    }


//
//    fun replaceScreen(screen: ru.terrakok.cicerone.Screen) {
//        router.replaceScreen(screen)
//    }
}