package com.davud.tutorials.mvp.auth

import com.arellomobile.mvp.MvpView

interface AuthView : MvpView {
    fun show(msg: String?)
}