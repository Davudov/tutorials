package com.davud.tutorials.ui.auth


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.davud.tutorials.R
import kotlinx.android.synthetic.main.ex1.*

class Example1 : Fragment() {
    companion object {
        @JvmStatic
        fun getInstance(string: String): Fragment {
            val args = Bundle()
            args.putString("key", string)
            return Example1().apply { this.arguments = args }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return View.inflate(context, R.layout.ex1, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        text.text = arguments?.getString("key", "null")
    }


}
